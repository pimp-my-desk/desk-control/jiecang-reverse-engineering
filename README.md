# Jiecang Reverse Engineering


This project is a compilation of information on the protocol of Jiecang's desks.
 I'm only focused on the RJ-12 port, I might be wrong but I believe this port in software terms to be High Level, so I consider it safer to mess with. When messing with this type of devices making the desk go to a higher or lower hight than possible might be a problem. Besides this I consider that using this port has the lowest risk of voiding your warranty, braking your desk or loosing the controller while attempting whatever you want. Just connect a telephone cable to the controller and connect the wires to the correct pins of your control device.


## Index
- [Pinout](#pinout)
- [Protocol](#protocol)
  - [Source](#source)

## Pinout

Well since we don't want to short anything and not all of us own osciloscopes the FCC pictures tell us that there are 2 versions of a bluetooth adapter, the newest one even has a new pin labeled BKG (background?) 

[Jiecang @ FCC](https://fcc.report/company/Zhejiang-Jiecang-Linear-Motion-Technology-Co-L-T-D)

| [JCP35NBLT](https://fcc.report/FCC-ID/2ANKDJCP35NBLT/) | [JCP35NBLT1](https://fcc.report/FCC-ID/2ANKDJCP35NBLT1/) |
| ------ | ------ |
|![Outside](JCP35NBLT/JCP35NBLT.png "Outside")|![Outside](JCP35NBLT1/JCP35NBLT1.png "Outside")|
|![Connector Above](JCP35NBLT/JCP35NBLTconnectorAbove.png "Connector Above")|![Connector Above](JCP35NBLT1/JCP35NBLT1connectorAbove.png "Connector Above")|
|![Connector Under](JCP35NBLT/JCP35NBLTconnectorUnder.png "Connector Under")|![Connector Under](JCP35NBLT1/JCP35NBLT1connectorUnder.png "Connector Under")|
|![Internal 1](JCP35NBLT/JCP35NBLTinternal1.png "Internal 1")|![Internal 1](JCP35NBLT1/JCP35NBLT1internal1.png "Internal 1")|
|![Internal 2](JCP35NBLT/JCP35NBLTinternal2.png "Internal 2")|![Internal 1](JCP35NBLT1/JCP35NBLT1internal2.png "Internal 2")|
|![Internal 3](JCP35NBLT/JCP35NBLTinternal3.png "Internal 3")||
|![Internal 4](JCP35NBLT/JCP35NBLTinternal4.png "Internal 4")||

## Protocol

|  DESCRIPTION      |DEVICE|CMD |LEN |  DATA  |CSUM|EoM |
| ----------------- |------|----|----| ------ |----|----|
|UP                 |0xF1F1|0x01|0x00|0x00    |0x01|0x7E|
|DOWN               |0xF1F1|0x02|0x00|0x00    |0x02|0x7E|
|MEMORY 1           |0xF1F1|0x03|0x00|0x00    |0x03|0x7E|
|MEMORY 2           |0xF1F1|0x04|0x00|0x00    |0x04|0x7E|
|GO TO MEMORY 1     |0xF1F1|0x05|0x00|0x00    |0x05|0x7E|
|GO TO MEMORY 2     |0xF1F1|0x06|0x00|0x00    |0x06|0x7E|
|FETCH_HEIGHT_VALUE |0xF1F1|0x07|0x00|0x00    |0x07|0x7E|
|                   |      |    |    |        |    |    |
|FETCH_HEIGHT_RANGE |0xF1F1|0x0C|0x00|0x00    |0x0C|0x7E|
|                   |      |    |    |        |    |    |
|GO TO HEIGHT       |0xF1F1|0x1B|0x02|[mm]/256 (division) & [mm]%256 (modulus) |VARIABLE|0x7E|
|                   |      |    |    |        |    |    |
|FETCH_HIGHLOW_LIMIT|0xF1F1|0x20|0x00|0x00    |0x20|0x7E|
|                   |      |    |    |        |    |    |
|MEMORY 3           |0xF1F1|0x25|0x00|0x00    |0x25|0x7E|
|                   |      |    |    |        |    |    |
|GO TO MEMORY 3     |0xF1F1|0x27|0x00|0x00    |0x27|0x7E|
|                   |      |    |    |        |    |    |
|STOP               |0xF1F1|0x2B|0x00|0x00    |0x2B|0x7E|
|                   |      |    |    |        |    |    |
|PATCH              |0xF1F1|0xA0|0x00|0x00    |0xA0|0x7E|
|                   |      |    |    |        |    |    |
|FETCH_STAND_TIME   |0xF1F1|0xA2|0x00|0x00    |0xA2|0x7E|
|                   |      |    |    |        |    |    |
|FETCH_ALL_TIME     |0xF1F1|0xAA|0x00|0x00    |0xAA|0x7E|
|                   |      |    |    |        |    |    |
|SET MEMORY HEIGHT  |0xF1F1|0xAD|0x03|0x01-0x03 & Height in mm in base 10 to base 16 in 2 Bytes    |VARIABLE|0x7E|

### Source
Discovering the protocol involves acting as the middle man between the desk controller on the RJ-45 port and decoding the meaning of the messages, or if you like puzzles you can decompile the dongle's app and reverse engineer the code, there are many tools for this include online ones.

The app can be downloaded via the link embedded in the QR code present in the [User's Manual for JCP35NBLT1](https://fcc.report/FCC-ID/2ANKDJCP35NBLT1/5897963)

Current discovered messages

Main Messages
Note: the patch command appears to be to change settings, Time seems to be related to health conserns with sitting/stand time, does the desk has an rtc? what's the advantage? this can be implemented in HA

```
cmdDown = "F1F10200027E";
cmdFetchAllTime = "F1F1AA00AA7E";
cmdFetchHeightRange = "F1F10C000C7E";
cmdFetchHeightValue = "F1F10700077E";
cmdFetchHighestLowestLimit = "F1F12000207E";
cmdFetchStandTime = "F1F1A60100A77E";
cmdGotoMemory1 = "F1F10500057E";
cmdGotoMemory2 = "F1F10600067E";
cmdGotoMemory3 = "F1F12700277E";
cmdMemory1 = "F1F10300037E";
cmdMemory2 = "F1F10400047E";
cmdMemory3 = "F1F12500257E";
cmdPatch = "F1F1A000A07E";
cmdStop = "F1F12B002B7E";
cmdUp = "F1F10100017E";
```
Confirmed sends the desk to specific hight in mm with soft-stop
```
cmdGoTo(int i) [in mm in my case]
    byte[] bArr = new byte[8];
    bArr[0] = (byte) Integer.parseInt("F1", 16); -> ID
    bArr[1] = (byte) Integer.parseInt("F1", 16); -> ID
    bArr[2] = (byte) Integer.parseInt("1B", 16); -> Cmd Code
    bArr[3] = (byte) Integer.parseInt(MyApplication.ERROR_CODE_E02, 16); -> Equals "02", don't know why
    bArr[4] = (byte) Integer.parseInt((i / 256) + "", 10); -> Division
    bArr[5] = (byte) Integer.parseInt((i % 256) + "", 10); -> Modulus
    bArr[6] = (byte) Integer.parseInt(((((bArr[2] + bArr[3]) + bArr[4]) + bArr[5]) % 256) + "", 10); -> Checksum
    bArr[7] = (byte) Integer.parseInt("7E", 16); -> EoM
```

Possibly sets the memory presets to specific heights

```
SharedPreferencesTools.setHeightStand(getApplicationContext(), this.choosedHeightStand);
            SharedPreferencesTools.setHeightSit(getApplicationContext(), this.choosedHeightSit);
            SharedPreferencesTools.setHeightRest(getApplicationContext(), this.choosedHeightRest);
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {
                    if (SharedPreferencesTools.getHeightStand(SetDeskActivity.this.getApplicationContext()) != 0) {
                        String upperCase = Integer.toHexString(Integer.valueOf(SharedPreferencesTools.getHeightStand(SetDeskActivity.this.getApplicationContext())).intValue()).toUpperCase();
                        String substring = ("0000" + upperCase).substring(upperCase.length(), upperCase.length() + 4);
                        Context applicationContext = SetDeskActivity.this.getApplicationContext();
                        StringBuilder sb = new StringBuilder();
                        sb.append("F1F1AD0301");
                        sb.append(substring);
                        sb.append(StringUtil.makeChecksum("AD0301" + substring));
                        sb.append("7E");
                        MainActivity.sendCommand(applicationContext, sb.toString());
                    }
                }
            }, 0);
            handler.postDelayed(new Runnable() {
                public void run() {
                    if (SharedPreferencesTools.getHeightSit(SetDeskActivity.this.getApplicationContext()) != 0) {
                        String upperCase = Integer.toHexString(Integer.valueOf(SharedPreferencesTools.getHeightSit(SetDeskActivity.this.getApplicationContext())).intValue()).toUpperCase();
                        String substring = ("0000" + upperCase).substring(upperCase.length(), upperCase.length() + 4);
                        Context applicationContext = SetDeskActivity.this.getApplicationContext();
                        StringBuilder sb = new StringBuilder();
                        sb.append("F1F1AD0302");
                        sb.append(substring);
                        sb.append(StringUtil.makeChecksum("AD0302" + substring));
                        sb.append("7E");
                        MainActivity.sendCommand(applicationContext, sb.toString());
                    }
                }
            }, 300);
            handler.postDelayed(new Runnable() {
                public void run() {
                    if (SharedPreferencesTools.getHeightRest(SetDeskActivity.this.getApplicationContext()) != 0) {
                        String upperCase = Integer.toHexString(Integer.valueOf(SharedPreferencesTools.getHeightRest(SetDeskActivity.this.getApplicationContext())).intValue()).toUpperCase();
                        String substring = ("0000" + upperCase).substring(upperCase.length(), upperCase.length() + 4);
                        Context applicationContext = SetDeskActivity.this.getApplicationContext();
                        StringBuilder sb = new StringBuilder();
                        sb.append("F1F1AD0303");
                        sb.append(substring);
                        sb.append(StringUtil.makeChecksum("AD0303" + substring));
                        sb.append("7E");
                        MainActivity.sendCommand(applicationContext, sb.toString());
                    }
                }
            }, 500);
```

